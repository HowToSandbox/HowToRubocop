
## How to Set up Rubocop 


#### Setup Sublime to work with Rubocop
- In sublime, open preferances, and select'syntax specific - User'
- Rename it to Ruby.sublime-settings
- Copy the code below

```ruby
{
    "default_encoding": "UTF-8",
    "default_line_ending": "unix",
    "ensure_newline_at_eof_on_save": true,
    "rulers":
    [
        80
    ],
    "tab_size": 2,
    "translate_tabs_to_spaces": true,
    "trim_trailing_white_space_on_save": true,
    "word_wrap": "true"
}
```


##### Installing [Sublimelinter-rubocop](https://github.com/SublimeLinter/SublimeLinter-rubocop "Title")
* Install sublime [package manager] (https://packagecontrol.io/installation "Title") 
* Open Package Manager
	* ctrl+shift+p (Win, Linux)
	* cmd+shift+p (OSX)
* Search for:

```` bash
Package Control: Install package
Sublimelinter-rubocop
```

####  Set up Rubocop in your repository
 - Add to Gemfile and bundle

```ruby 
 	gem 'rubocop', require: false  
```
- Install manually for quick and dirty linting

```bash
$ gem install rubocop
```

###### Setup Rubocop configuration
- add .rubocop.yml to base of repo
- add disabled.yml to base of repo
- add enabled.yml to base of repo

Rubocop default examples [here:](https://github.com/bbatsov/rubocop/blob/master/config/default.yml "Title")


#### Running Rubocop
- run from the command line

```bash
$ cd /project/dir
$ rubocop
```
Rubocop will scan all the files and folders starting from the TLD where you ran it.
#### Sauce
https://buildtoship.com/integrate-rubocop-in-your-workflow/

http://joshfrankel.me/blog/2015/how_to/how-to-configure-sublime-text-3-for-rubocop-and-ruby-coding-standards/

https://github.com/SublimeLinter/SublimeLinter-rubocop

https://github.com/bbatsov/rubocop/blob/master/config/default.yml



